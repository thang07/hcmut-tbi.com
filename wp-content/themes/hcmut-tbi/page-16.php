<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 12/10/14
 * Time: 1:32 PM
 * trang tu lieu
 */
get_header();?>
<div class="news-page events-pages">
    <section id="newsall">
        <div class="container round-news cta-text">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="fonts">
                        <?php echo get_the_title(16);?>
                    </h2>
                </div>
            </div>
            <div class="row">

                <div class="col-sm-12 col-md-12">

                    <?php
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    global $post;
                    $args = array('numberposts' => 100, 'category_name' => 'tin-tuc',
                        'posts_per_page' => 4,
                        'order' => 'DESC',
                        'paged' => $paged,
                    );
                    $posts = new WP_Query($args);
                    while ($posts->have_posts()):$posts->the_post();
                        ?>
                        <div class="col-sm-6 col-md-6 news">
                            <div class="media post-item">
                                <a href="<?php echo the_permalink()?>">
                                    <?php echo the_post_thumbnail(); ?>
                                </a>

                                <div class="media-body">
                                    <div class="fonts-reds"><a href="<?php echo the_permalink()?>"><?php echo the_title(); ?></a></div>
                                    <div class="details">
                                        <?php echo wp_trim_words(get_the_excerpt(), 18); ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php endwhile ?>
                </div>
                <div class="col-md-12">
                    <br/> <br/>

                    <div class="pavigation">
                        <?php
                        $big = 999999999; // need an unlikely integer
                        echo paginate_links(array(
                            'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                            'format' => '?paged=%#%',
                            'current' => max(1, get_query_var('paged')),
                            'total' => $posts->max_num_pages));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>
