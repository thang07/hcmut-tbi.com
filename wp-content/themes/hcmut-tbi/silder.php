<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 11/24/14
 * Time: 10:12 AM
 */ ?>
<section id="featured">
    <!-- start slider -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Slider -->
                <div id="main-slider" class="flexslider">
                    <ul class="slides">
                        <?php
                        global $post;

                        $array = array('category_name' => 'slides', 'orderrand' => 'desc', 'numberposts' => '15');
                        $result = get_posts($array);
                        foreach ($result as $post): setup_postdata($post);

                        ?>
                        <li>

                            <?php echo get_the_post_thumbnail();?>
                            <div class="flex-caption">
<!--                                <h3 class="font-color-white">--><?php //echo the_title();?><!--</h3>-->
<!--                                <p>--><?php //echo $content= the_content() ;?><!--</p>-->
                                <a href="<?php the_permalink();?>" class="btn btn-theme">
                                    <?php if(isset($_GET['lang'])){?>
                                        Learn More
                                    <?php }else{?>
                                        Xem thêm
                                    <?php  } ?>

                                </a>
                            </div>
                        </li>
                        <?php endforeach ?>

                    </ul>
                </div>
                <!-- end slider -->
            </div>
        </div>
    </div>

</section>