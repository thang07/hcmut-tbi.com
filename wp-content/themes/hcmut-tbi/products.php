<section id="productsall">
    <div class="container">
        <div class="row">
            <div class=" col-sm-12 col-md-12">
                <h2 class="heading fonts">
                    <?php if(isset($_GET['lang'])){?>
                    <a title="View more" href="<?php echo get_permalink( 142 );?>">  Customers recent</a>
                    <?php }else{?>
                       <a title="Xem thêm" href="<?php echo get_permalink( 142 );?>"> DOANH NGHIỆP TIÊU BIỂU</a>
                    <?php  } ?>

                </h2>

                <div id="projects">
                    <ul id="thumbs" class="portfolio banner">

                        <?php
                        global $post;
                        $array = array('category_name' => 'customers', 'orderrand' => 'desc', 'numberposts' => '6');
                        $result = get_posts($array);
                        foreach ($result as $post): setup_postdata($post);
                            $i=0;
                        ?>
                        <li class="col-sm-4 col-md-4 design <?php if($i%3==0){echo 'item-three';} ?> " data-id="id-0" data-type="web">
                            <div class="item-thumbs">

                                <a class="hover-wrap "  title="Product 1"
                                   href="<?php echo the_permalink()?>">
                                    <span class="overlay-img"><?php echo the_title()?></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <?php echo get_the_post_thumbnail(); ?>

                            </div>
                        </li>

                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</section>
