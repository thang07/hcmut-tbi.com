<?php get_header()?>
<div class="container">
    <div class="col-md-12">
        <div class="col-md-3">
            <div class="icon"><i class="fa fa-graduation-cap fa-3x"></i></div>
            <p style="text-align: center;">Nâng cao nhận thức của cộng đồng về ươm tạo doanh nghiệp và công
                nghệ</p>
        </div>
        <div class="col-md-3">
            <div><i class="fa fa-certificate fa-3x"></i></div>
            <p style="text-align: center;">Nâng cao năng lực, hiệu quả hoạt động của Trung tâm</p>
        </div>
        <div class="col-md-3">
            <div><i class="fa fa-graduation-cap fa-3x"></i></div>
            <p style="text-align: center;">Tạo ra nhiều doanh nghiệp khoa học và công nghệ được ươm tạo từ
                Trung tâm</p>
        </div>
        <div class="col-md-3">
            <div><i class="fa fa-globe fa-3x"></i></div>
            <p style="text-align: center;">Quảng bá, nhân rộng mô hình doanh nghiệp Khoa học và công nghệ tới
                các doanh nghiệp và các đối tượng liên quan.</p>
        </div>
    </div>
</div>
<?php get_footer()?>