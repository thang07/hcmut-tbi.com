<!-- End of head section HTML codes -->
<section id="partners">
    <div class="container">
        <h2 class="fonts">
            <a title="Xem thêm" href="<?php echo get_category_link(3) ?>"> <?php echo get_cat_name(3) ?>
                <?php if (isset($_GET['lang'])) { ?>
                    PARTNER
                <?php } else { ?>
                    LIÊN KẾT
                <?php } ?>
            </a>
        </h2>
    </div>
    <div style="margin:0px auto;">    <!-- Insert to your webpage where you want to display the carousel -->

        <div id="amazingcarousel-container-1">
            <div id="amazingcarousel-1"
                 style="display:block;position:relative;width:100%;max-width:990px;margin:0px auto 0px;">
                <div class="amazingcarousel-list-container">
                    <ul class="amazingcarousel-list">
                        <?php
                        global $post;
                        $array = array('category_name' => 'partners', 'orderrand' => 'desc', 'numberposts' => '20');
                        $result = get_posts($array);
                        foreach ($result as $post): setup_postdata($post);
                            ?>
                            <li class="amazingcarousel-item">
                                <div class="amazingcarousel-item-container">
                                    <div class="amazingcarousel-image">
                                        <?php if (has_post_thumbnail()) { ?>
                                            <a href="<?php echo get_field('url') ?>" data-group="amazingcarousel-1"
                                               class="html5lightbox" title="<?php echo the_title() ?>" target="_blank">
                                                <?php echo the_post_thumbnail(); ?>
                                            </a>
                                        <?php } else { ?>
                                            <div class="img">
                                                <div class="imgg">
                                                    <?php echo the_title() ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach ?>
                    </ul>
                    <div class="amazingcarousel-prev"></div>
                    <div class="amazingcarousel-next"></div>
                </div>

            </div>
        </div>
</section>
<!-- End of body section HTML codes -->
