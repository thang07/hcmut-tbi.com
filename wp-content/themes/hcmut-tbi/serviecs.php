<section class="services-index">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="big-cta">
                    <div class="cta-text">
                        <?php if (isset($_GET['lang'])) { ?>
                            <h2 class="fonts"><span>Services</span> & Resource</h2>
                        <?php } else { ?>
                            <h2 class="fonts">NHIỆM VỤ TRỌNG TÂM</h2>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="content" class="services">
    <div class="container">
        <div class="row">
            <div class=" col-sm-12 col-md-12">

                <div class="col-sm-4 col-md-4 item">
                    <div class="box">
                        <div class="box-gray aligncenter">
                            <div class="icon">
                                <i class="fa fa-edit fa-3x"></i>
                            </div>
                            <?php
                            $post = get_post(665);
                            $content = apply_filters('the_content', $post->post_content);
                            $titl = apply_filters('the_title', $post->post_title);
                            ?>
                            <h4 class="fonts-h4"><a href="<?php echo get_permalink(38) ?>"><?php echo $titl ?></a></h4>

                            <div class="ser">
                                <?php echo $content; ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-sm-4 col-md-4 item">
                    <div class="box">
                        <div class="box-gray aligncenter">

                            <div class="icon">
                                <i class="fa fa-pagelines fa-3x"></i>
                            </div>
                            <?php
                            $post2 = get_post(667);
                            $content2 = apply_filters('the_content', $post2->post_content);
                            $titl2 = apply_filters('the_title', $post2->post_title);
                            ?>
                            <h4 class="fonts-h4"><a href="<?php echo get_permalink(42) ?>"> <?php echo $titl2; ?></a></h4>

                            <div class="ser">
                                <?php echo $content2; ?>

                            </div>
                        </div>
                    </div>
                </div>

                <div class=" col-sm-4 col-md-4 item">
                    <div class="box">
                        <div class="box-gray aligncenter">


                            <div class="icon">
                                <i class="fa fa-desktop fa-3x"></i>
                            </div> <?php
                            $post3 = get_post(669);
                            $content3 = apply_filters('the_content', $post3->post_content);
                            $titl3 = apply_filters('the_title', $post3->post_title);
                            ?>

                            <h4 class="fonts-h4"><a href="<?php echo get_permalink(40) ?>"><?php echo $titl3; ?></a></h4>

                            <div class="ser">
                                <?php echo $content3; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>