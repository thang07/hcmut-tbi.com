<?php
/**
 * The template for displaying an event-category page
 *
 ***************** NOTICE: *****************
 *  Do not make changes to this file. Any changes made to this file
 * will be overwritten if the plug-in is updated.
 *
 * To overwrite this template with your own, make a copy of it (with the same name)
 * in your theme directory. See http://docs.wp-event-organiser.com/theme-integration for more information
 *
 * WordPress will automatically prioritise the template in your theme directory.
 ***************** NOTICE: *****************
 *
 * @package Event Organiser (plug-in)
 * @since 1.0.0
 */

//Call the template header
get_header(); ?>

<div class="events-pages">
    <div class="container">
        <div class="row">
            <div class="col-md-12">


                <div class="col-md-8">
                    <div id="primary" class="eventscate">
                        <h2 class="fonts-h2 text-center">
                            <?php if(isset($_GET['lang'])){?>
                                Events
                            <?php }else{?>
                                Sự Kiện
                            <?php  } ?>

                        </h2>

                        <?php if (have_posts()) : ?>
                            <!-- Page header, display category title-->
                            <div class="">
                                <!-- If the category has a description display it-->
                                <?php
                                $category_description = category_description();
                                if (!empty($category_description))
                                    echo apply_filters('category_archive_meta', '<div class="category-archive-meta">' . $category_description . '</div>');
                                ?>
                            </div>

                            <!-- Navigate between pages-->
                            <!-- In TwentyEleven theme this is done by twentyeleven_content_nav-->
                            <?php
                            global $wp_query;
                            if ($wp_query->max_num_pages > 1) : ?>
                                <nav id="nav-above">
                                    <div
                                        class="nav-next events-nav-newer"><?php next_posts_link(__('Later events <span class="meta-nav">&rarr;</span>', 'eventorganiser')); ?></div>
                                    <div
                                        class="nav-previous events-nav-newer"><?php previous_posts_link(__(' <span class="meta-nav">&larr;</span> Newer events', 'eventorganiser')); ?></div>
                                </nav><!-- #nav-above -->
                            <?php endif; ?>

                            <?php /* Start the Loop */ ?>
                            <?php while (have_posts()) : the_post(); ?>
                                <div class="number-line">
                                    <span class="icon"><i class="fa fa-calendar-o"></i></span>
                                    <span class="text">
                                       <?php if (eo_is_all_day()) {
                                           $format = 'd F Y';
                                           $microformat = 'Y-m-d';
                                           $date='d';
                                           $month='M';
                                           $year='y';
                                       } else {
                                           $format = 'd F Y ' . get_option('time_format');
                                           $microformat = 'c';
                                           $date='d';
                                           $month='M';
                                           $year='y';
                                       }?>
                                        <?php echo eo_the_start($date);?> / <?php echo eo_the_start($month);?> / <?php echo eo_the_start($year);?>
                                    </span>
                                </div>

                                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                    <header class="entry-header">

                                        <div class="entry-title" style="display: inline;">
                                            <a href="<?php the_permalink(); ?>" class="a-img">
                                                <?php
                                                //If it has one, display the thumbnail
                                                // if (has_post_thumbnail())
                                                //    the_post_thumbnail('thumbnail', array('style' => 'float:left;margin-right:20px;'));

                                                //Display the title
                                                the_title();?>
                                            </a>
                                        </div>

                                        <div class="event-entry-meta">

                                            <!-- Output the date of the occurrence-->
                                            <?php
                                            //Format date/time according to whether its an all day event.
                                            //Use microdata https://support.google.com/webmasters/bin/answer.py?hl=en&answer=176035
                                            if (eo_is_all_day()) {
                                                $format = 'd M Y';
                                                $microformat = 'Y-m-d';
                                            } else {
                                                $format = 'd/m/Y ' . get_option('time_format');
                                                $microformat = 'c';
                                            }?>
                                            <time itemprop="startDate"
                                                  datetime="<?php eo_the_start($microformat); ?>">
                                                <i class="fa fa-clock-o"></i> :  <?php eo_the_start($format); ?>
                                            </time>
                                            <div> <i class="font-icon-map-marker"></i>

                                                : <?php echo eo_get_venue_name();?></div>

                                            <!-- Display event meta list -->
                                            <?php //echo eo_get_event_meta_list(); ?>

                                            <!-- Show Event text as 'the_excerpt' or 'the_content' -->
                                            <div class="events-excerpt">
                                                <?php echo the_excerpt(); ?>

                                            </div>
                                        </div>
                                    </header>


                                </article><!-- #post-<?php// the_ID(); ?> -->

                            <?php endwhile; ?><!--The Loop ends-->


                            <?php
                            if ($wp_query->max_num_pages > 1) : ?>
                                <nav id="nav-below">
                                    <div
                                        class="nav-next events-nav-newer"><?php next_posts_link(__('Later events <span class="meta-nav">&rarr;</span>', 'eventorganiser')); ?></div>
                                    <div
                                        class="nav-previous events-nav-newer"><?php previous_posts_link(__(' <span class="meta-nav">&larr;</span> Newer events', 'eventorganiser')); ?></div>
                                </nav><!-- #nav-below -->
                            <?php endif; ?>

                        <?php else : ?>

                            <!-- If there are no events -->
                            <article id="post-0" class="post no-results not-found">
                                <header class="entry-header">
                                    <h1 class="entry-title"><?php _e('Nothing Found', 'eventorganiser'); ?></h1>
                                </header>
                                <!-- .entry-header -->

                                <div class="entry-content">
                                    <p><?php _e('Apologies, but no events were found for the requested category. ', 'eventorganiser'); ?></p>
                                </div>
                                <!-- .entry-content -->
                            </article><!-- #post-0 -->

                        <?php endif; ?>

                    </div>
                    <!-- #content -->
                </div>
                <div class="col-md-4">
                    <div class="slide-can sidebar-nav-fixed ">
                        <?php if(isset($_GET['lang'])){?>
                            <h2 class="fonts">Calendar</h2>
                        <?php }else{?>
                            <h2 class="fonts">Lịch</h2>
                        <?php }?>
                        <?php if (is_active_sidebar('right-sidebar')) echo dynamic_sidebar('right-sidebar'); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php get_footer(); ?>
