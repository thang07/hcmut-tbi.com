<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
    <div class="page-single events-pages">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-9 deatils">
                        <div class="panel panel-default">
                            <?php if (have_posts()) :

                                while (have_posts()) : the_post();?>
                                    <?php setPostViews(get_the_ID()); ?>
                                    <div class="single-title">
                                        <i class="fa fa-home"></i>
                                        <a href="<?php bloginfo('home'); ?>"
                                           class="home"><?php _e('Home'); ?></a> &raquo;
                                        <?php echo the_category(','); ?>
                                        <div class="time row">
                                            <div class="col-md-6">
                                                <?php the_time('d/m/Y'); ?> <?php _e('At'); ?>  <?php the_time('H:m'); ?>
                                                - View: <?php echo getPostViews(get_the_ID()) ?>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <div id="fb-root"></div>
                                                <script>(function(d, s, id) {
                                                        var js, fjs = d.getElementsByTagName(s)[0];
                                                        if (d.getElementById(id)) return;
                                                        js = d.createElement(s); js.id = id;
                                                        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&appId=278177195639679&version=v2.0";
                                                        fjs.parentNode.insertBefore(js, fjs);
                                                    }(document, 'script', 'facebook-jssdk'));</script>
                                                <div class="fb-like" data-href="<?php echo the_permalink();?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <h4 class="title"><?php the_title() ?></h4>
                                        <?php echo the_content(); ?>
                                    </div>

                                <?php endwhile; ?>
                                <div class="relation-post">
                                    <h4> <?php if(isset($_GET['lang'])){echo "Related posts:";}else{ echo "Bài liên quan:";}?> </h4>
                                    <ul>
                                        <?php

                                        $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 5, 'post__not_in' => array($post->ID) ) );
                                        if( $related ) foreach( $related as $post ) {
                                            setup_postdata($post); ?>

                                                <li>
                                                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>

                                                </li>

                                        <?php }
                                        wp_reset_postdata(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-right-single">

                        <?php include 'top-new-viewer.php' ?>
                        <?php include 'find-us-facebook.php' ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php

get_footer();
