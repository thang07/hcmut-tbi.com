<!DOCTYPE html>
<!--[if IE 7]>
<html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9]>
<html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en"> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" type="image/x-icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="truong dai hoc bach khoa, dai hoc bach khoa, bach khoa, vuon uom, khu vuon uom, khoa vuon uom"/>

    <!-- css -->
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen"/>
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri(); ?>/css/flexslider.css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/carouselengine/initcarousel-1.css">

    <!-- Theme skin -->
    <link href="<?php echo get_template_directory_uri(); ?>/skins/default.css" rel="stylesheet"/>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="js/html5.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper">
    <!-- start header -->
    <header>
        <div class="navbar navbar-default navbar-static-top navbar-fixed-top menuheader">
            <div class="container">
                <div class="row row-head">

                    <div class="">
                        <div class="col-xs-2 col-sm-2 col-md-2 img-logo">
                            <h1 class="name-site" style="visibility: hidden;display: none" title="<?php echo $blog_title = get_bloginfo('name'); ?>"><?php echo $blog_title = get_bloginfo('name'); ?><h1>
                            <a  href=" <?php echo get_home_url(); ?>">
                                <img src="<?php echo get_template_directory_uri() ?>/img/logos.png" style="height: 89px"
                                     width="90" class="img-responsive logos" alt="logo"/>
                            </a>
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-9 logan">
                            <?php if (isset($_GET['lang'])) { ?>
                                <img src="<?php echo get_template_directory_uri() ?>/img/logo_en.jpg" class="img-responsive" width="598px" height="83px" alt="logan"/>

                            <?php } else { ?>
                                <img src="<?php echo get_template_directory_uri() ?>/img/logo_vi.jpg" class="img-responsive " width="598px" height="83px" alt="logan"/>

                            <?php } ?>

                        </div>
                        <div class="col-xs-1 col-sm-1 col-md-1 langu">
                            <div class="languages">
                                <?php qtrans_generateLanguageSelectCode('image'); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>
                <div class="navbar-collapse collapse ">
                    <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'nav navbar-nav')); ?>

                </div>
            </div>
        </div>
        <div class="navbar navbar-default navbar-static-top navbar-fixed-top menubot">
            <div class="container">
                <div class="row row-head">
                    <div class="langu">
                        <div class="languages">
                            <?php qtrans_generateLanguageSelectCode('image'); ?>

                        </div>
                    </div>

                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                        <img src="<?php echo get_template_directory_uri() ?>/img/logos.png" style="height: 89px"
                             width="90" class="img-responsive logos" alt="logo"/>
                    </a>
                </div>
                <div class="navbar-collapse collapse ">
                    <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'nav navbar-nav')); ?>
                </div>
            </div>
        </div>
    </header>
    <!-- end header -->