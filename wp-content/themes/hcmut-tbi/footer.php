﻿<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8">
                    <div class="widget">
                        <div class="widgetheading"> <?php echo get_the_title(294) ?></div>
                        <div class="address">
                            <a href=" <?php get_home_url(); ?> ">
                                <img src="<?php echo get_template_directory_uri() ?>/img/logos.png"
                                     class="img-responsive footer-logo" alt="71x71" height="71" width="71"/>
                            </a>

                            <div class="list-group">
                                <?php $post = get_post(294);
                                $content = apply_filters('the_content', $post->post_content);
                                echo $content; ?>
                                <div class="totalhist">

                                    <?php  dynamic_sidebar('visiters-sidebar'); ?>

<!--                                    --><?php
//                                    $bataswaktu       = time() - 300;
//                                    $pengunjungonline = mysql_num_rows(mysql_query("SELECT * FROM `" . BMW_TABLE_NAME . "` WHERE online > '$bataswaktu'"));
//
//
//                                    $totalhits = mysql_result(mysql_query("SELECT SUM(hits) FROM `" . BMW_TABLE_NAME . "`"), 0);
//                                    echo "<b> Total hit: </b>" . $totalhits;
//
//                                    echo "- <b>  Online: </b> " . $pengunjungonline;
//                                    ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="widget">
                        <div class="widgetheading">
                            <?php if (isset($_GET['lang'])) { ?>
                                CONTACT US
                            <?php } else { ?>
                                LIÊN HỆ
                            <?php } ?>

                        </div>
                        <p>
                            <?php if (isset($_GET['lang'])) { ?>
                                Find out about our latest blog entries, products and projects.
                            <?php } else { ?>
                                Liên hệ với chúng tôi ngay để được giải đáp thắc mắc của bạn.
                            <?php } ?>

                        </p>

                        <div class="subride">
                            <script>
                                function myFunction() {
                                    location.replace("<?php echo get_home_url()?>/?page_id=162<?php if(isset($_GET['lang'])){echo '&lang=en';}?>");
                                }
                            </script>

                            <input class="email form-control" type="email" placeholder="Email:"/>
                            <input type="submit" onclick=" myFunction()" value=" <?php if (isset($_GET['lang'])) { ?>
                               Send email
                            <?php } else { ?>
                               Liên hệ
                            <?php } ?>" class="btn btn-danger"/>

                        </div>
                        <ul class="social-network">
                            <li><a href="https://www.facebook.com/hcmut.tbi/" target="_blank" data-placement="top"
                                   title="Facebook"><i
                                        class="fa fa-facebook"></i></a></li>
                            <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                            </li>

                            <li><a href="google.com" data-placement="top" title="Google plus"><i
                                        class="fa fa-google-plus"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<?php wp_footer(); ?>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="--><?php //echo get_template_directory_uri(); ?><!--/js/jquery.js"></script>-->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox.pack.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox-media.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/google-code-prettify/prettify.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/portfolio/jquery.quicksand.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/portfolio/setting.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/animate.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/carouselengine/amazingcarousel.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/carouselengine/initcarousel-1.js"></script>

</body>
</html>