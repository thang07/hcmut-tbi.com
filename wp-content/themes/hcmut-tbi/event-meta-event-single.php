<?php
/**
 * The template is used for displaying a single event details.
 *
 * You can use this to edit how the details re displayed on your site. (see notice below).
 *
 * Or you can edit the entire single event template by creating a single-event.php template
 * in your theme.
 *
 * For a list of available functions (outputting dates, venue details etc) see http://codex.wp-event-organiser.com
 *
 ***************** NOTICE: *****************
 *  Do not make changes to this file. Any changes made to this file
 * will be overwritten if the plug-in is updated.
 *
 * To overwrite this template with your own, make a copy of it (with the same name)
 * in your theme directory. See http://docs.wp-event-organiser.com/theme-integration for more information
 *
 * WordPress will automatically prioritise the template in your theme directory.
 ***************** NOTICE: *****************
 *
 * @package Event Organiser (plug-in)
 * @since 1.7
 */
?>

<div class="entry-meta eventorganiser-event-meta">
    <!-- Choose a different date format depending on whether we want to include time -->
    <?php if (eo_is_all_day()) {
        $date_format = 'j F Y';
    } else {
        $date_format = 'j F Y ' . get_option('time_format');
    } ?>
    <hr>

    <!-- Event details -->
    <h4>
        <?php if (isset($_GET['lang'])) { ?>
            <?php _e('Event Details', 'eventorganiser'); ?>
        <?php } else { ?>
            <?php _e('Chi tiết', 'eventorganiser'); ?>
        <?php } ?>
    </h4>
    <!-- Is event recurring or a single event -->
    <ul class="eo-event-meta">
        <?php if (eo_reoccurs()): ?>
            <!-- Event reoccurs - is there a next occurrence? -->
            <?php $next = eo_get_next_occurrence($date_format); ?>

            <?php if ($next): ?>

                <li>
                    <strong>
                        <?php if (isset($_GET['lang'])) { ?>
                            <?php _e('Start', 'eventorganiser'); ?>
                        <?php } else { ?>
                            <?php _e('Bắt đầu', 'eventorganiser'); ?>
                        <?php } ?>
                        :</strong>
                    <?php
                    if (eo_is_all_day()) {
                        $format = 'd M Y ';
                        $microformat = ' Y-m-d ';
                    } else {
                        $format = ' d/m/Y ' . get_option('time_format');
                        $microformat = ' c ';
                    } ?>

                    <?php eo_the_start($format); ?>
                </li>



                <li>
                    <strong>
                        <?php if (isset($_GET['lang'])) { ?>
                            <?php _e(' End', 'eventorganiser'); ?>
                        <?php } else { ?>
                            <?php _e('Kết thúc', 'eventorganiser'); ?>
                        <?php } ?>
                        :</strong>

                    <?php echo eo_get_the_end('d/m/Y H:m a'); ?>
                </li>


            <?php else: ?>
                <?php if (isset($_GET['lang'])) { ?>
                    <?php printf('<p>' . __('This event finished on %s', 'eventorganiser') . '.</p>', eo_get_schedule_last('d F Y', '')); ?>

                <?php } else { ?>
                    <?php printf('<p>' . __('Sự kiện này đã hoàn thành vào %s', 'eventorganiser') . '.</p>', eo_get_schedule_last('d F Y', '')); ?>

                <?php } ?>

            <?php
            endif;
            ?>
        <?php endif; ?>



        <?php if (!eo_reoccurs()) { ?>
            <!-- Single event -->
            <li><strong>
                    <?php if (isset($_GET['lang'])) { ?>
                        <?php _e('Start', 'eventorganiser'); ?>
                    <?php } else { ?>
                        <?php _e('Bắt đầu', 'eventorganiser'); ?>
                    <?php } ?>
                    :</strong>

                <?php
                if (eo_is_all_day()) {
                    $format = 'd M Y ';
                    $microformat = ' Y-m-d ';
                } else {
                    $format = ' d/m/Y ' . get_option('time_format');
                    $microformat = ' c ';
                } ?>

                <?php eo_the_start($format); ?>
            </li>
            <li>
                <strong>
                    <?php if (isset($_GET['lang'])) { ?>
                        <?php _e(' End', 'eventorganiser'); ?>
                    <?php } else { ?>
                        <?php _e('Kết thúc', 'eventorganiser'); ?>
                    <?php } ?>
                    :</strong>

                <?php echo eo_get_the_end('d/m/Y H:m a'); ?>

            </li>
        <?php } ?>

        <?php if (eo_get_venue()) {
            $tax = get_taxonomy('event-venue'); ?>
            <li>
                <strong>
                    <?php if (isset($_GET['lang'])) { ?>
                        <?php _e(' Venue', 'eventorganiser'); ?>

                    <?php } else { ?>
                        <?php _e('Địa điểm', 'eventorganiser'); ?>
                    <?php } ?>
                    :</strong>
                <?php eo_venue_name(); ?>
            </li>
        <?php } ?>

        <?php if (eo_reoccurs()) {
            //Event reoccurs - display dates.
            $upcoming = new WP_Query(array(
                'post_type' => 'event',
                'event_start_after' => 'today',
                'posts_per_page' => -1,
                'event_series' => get_the_ID(),
                'group_events_by' => 'occurrence' //Don't group by series
            ));

            if ($upcoming->have_posts()): ?>

                <!--                <li><strong>--><?php //_e('Upcoming Dates', 'eventorganiser'); ?><!--:</strong>-->
                <!--                    <ul id="eo-upcoming-dates">-->
                <!--                        --><?php //while ($upcoming->have_posts()): $upcoming->the_post(); ?>
                <!--                            <li> --><?php //eo_the_start($date_format) ?><!--</li>-->
                <!--                        --><?php //endwhile; ?>
                <!--                    </ul>-->
                <!--                </li>-->

                <?php
                wp_reset_postdata();
                //With the ID 'eo-upcoming-dates', JS will hide all but the next 5 dates, with options to show more.
                wp_enqueue_script('eo_front');
                ?>
            <?php endif; ?>
        <?php } ?>

    </ul>

    <!-- Does the event have a venue? -->
    <?php if (eo_get_venue()): ?>
        <!-- Display map -->
        <div class="eo-event-venue-map">
            <?php echo eo_get_venue_map(eo_get_venue(), array('width' => '100%', 'height' => '140px')); ?>
        </div>
    <?php endif; ?>


    <div style="clear:both"></div>

    <hr>

</div><!-- .entry-meta -->
