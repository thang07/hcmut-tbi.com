<?php


//Call the template header
get_header(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.quick.pagination.min.js"
        type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {

        $("ul.pagination1").quickPagination({pageSize: "4", currentPage: 1, holder: null, pagerLocation: "after"});

    });
</script>
<div class="events-pages">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-8">
                    <div id="primary" class="eventscate">
                        <h2 class="fonts-h2 text-center">
                            <?php if (isset($_GET['lang'])) { ?>
                                Events
                            <?php } else { ?>
                                Sự Kiện
                            <?php } ?>

                        </h2>

                        <?php if (have_posts()) : ?>
                            <ul class="pagination1">
                            <?php /* Start the Loop */

                            $events = eo_get_events(array(
                                'numberposts' => 5,
                                'order' => 'desc',
                                'showpastevents' => true,//Will be deprecated, but set it to true to play it safe.
                            ));

                            if ($events):
                                foreach ($events as $event):setup_postdata($event);
                                    $format = (eo_is_all_day($event->ID) ? get_option('date_format') : get_option('date_format') . ' ' . get_option('time_format'));
                                    ?>
                                    <li class="">
                                        <div class="number-line">
                                            <span class="icon"><i class="fa fa-calendar-o"></i></span>
                                    <span class="text">
                                       <?php if (eo_is_all_day($event->ID)) {
                                           $format = 'd F Y';
                                           $microformat = 'Y-m-d';
                                           $date = 'd';
                                           $month = 'M';
                                           $year = 'Y';
                                       } else {
                                           //$format = 'd F Y ' . get_option('time_format');
                                           $format = 'd / F / Y ';
                                           $microformat = 'c';
                                           $date = 'd';
                                           $month = 'M';
                                           $year = 'Y';
                                       }?>
                                       <?php echo eo_get_the_start($format, $event->ID, null, $event->occurrence_id); ?>
                                       <?php// echo eo_the_start($month);
                                       ?>  <?php// echo eo_the_start($year);
                                       ?>
                                    </span>
                                        </div>

                                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                            <header class="entry-header">

                                                <div class="entry-title" style="display: inline;">
                                                    <a href="<?php echo get_permalink($event->ID); ?>" class="a-img">
                                                        <?php echo wp_trim_words(get_the_title($event->ID)); ?>
                                                    </a>
                                                </div>
                                                <div class="event-entry-meta">

                                                    <?php

                                                    if (eo_is_all_day()) {
                                                        $format = 'd M Y';
                                                        $microformat = 'Y-m-d';
                                                    } else {
                                                        $format = 'd/m/Y ' . get_option('time_format');
                                                        $microformat = 'c';
                                                    }?>


                                                    <time itemprop="startDate"
                                                          datetime="<?php eo_the_start($microformat, $event->ID, null, $event->occurrence_id); ?>">
                                                        <i class="fa fa-clock-o"></i>
                                                        :  <?php eo_the_start($format, $event->ID, null, $event->occurrence_id); ?>
                                                        <?php echo eo_get_the_end('- H:m a', $event->ID, null, $event->occurrence_id); ?>
                                                    </time>
                                                    <div><i class="font-icon-map-marker"></i>
                                                        <?php $venue_id = eo_get_venue($event->ID);

                                                        echo eo_get_venue_name($venue_id);?></div>

                                                    <!-- Display event meta list -->
                                                    <?php //echo eo_get_event_meta_list();
                                                    ?>

                                                    <!-- Show Event text as 'the_excerpt' or 'the_content' -->
                                                    <div class="events-excerpt">
                                                        <!--                                                --><?php //echo substr(get_the_excerpt(), 0,120);
                                                        ?>
                                                        <?php the_excerpt(); ?>
                                                    </div>
                                                </div>
                                            </header>

                                        </article>
                                    </li>
                                <?php
                                endforeach;?>
                                </ul>
                            <?php
                                // endwhile;
                            endif;
                        endif; ?>
                    </div>
                    <!-- #content -->
                </div>
                <div class="col-md-4">
                    <div class="slide-can sidebar-nav-fixed ">
                        <?php if (isset($_GET['lang'])) { ?>
                            <h2 class="fonts">Calendar</h2>
                        <?php } else { ?>
                            <h2 class="fonts">Lịch</h2>
                        <?php } ?>
                        <?php if (is_active_sidebar('right-sidebar')) echo dynamic_sidebar('right-sidebar'); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php get_footer(); ?>
