<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 12/21/14
 * Time: 11:08 PM
 */
get_header()?>
    <div class="page-customers events-pages">
        <div class="container">
            <div class="row">
                <ul class="col-sm-12 cate banner">
                    <h2 class="fonts"><a
                            title="<?php
                            if (isset($_GET['lang']))
                            {?> View more<?php } else {?>Xem thêm <?php } ?>" href="<?php echo get_category_link(6);?>"><?php echo get_cat_name(6) ?></a></h2>
                    <?php
                    global $post;
                    $args = array('numberposts' => 3, 'category_name' => 'doanh-nghiep-dang-uom-tao');
                    $posts = get_posts($args);

                    foreach ($posts as $post): setup_postdata($post);
                        ?>
                        <li class="col-sm-4">
                            <div class="item">
                                <a href="<?php echo the_permalink() ?>">
                                    <?php echo the_post_thumbnail() ?>
                                </a>
                                <h5 class="title"><?php echo the_title() ?></h5>
                            </div>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>

            <div class="row">
                <ul class="col-sm-12 cate banner">
                    <h2 class="fonts"><a
                            title="<?php
                        if (isset($_GET['lang']))
                        {?> View more<?php } else {?>Xem thêm <?php } ?>" href="<?php echo get_category_link(7);?>"><?php echo get_cat_name(7) ?></a></h2>
                    <?php
                    global $post;
                    $args = array('numberposts' => 3, 'category_name' => 'doanh-nghiep-da-tot-nghiep');
                    $posts = get_posts($args);

                    foreach ($posts as $post): setup_postdata($post);
                        ?>
                        <li class="col-sm-4">
                            <div class="item">
                                <a href="<?php echo the_permalink() ?>">
                                    <?php echo the_post_thumbnail() ?>
                                </a>
                                <h5 class="title"><?php echo the_title() ?></h5>
                            </div>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>

        </div>
    </div>
<?php get_footer() ?>