<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 12/25/14
 * Time: 11:24 AM
 */
get_header()?>
    <div class="page-652 events-pages">
        <div class="container">
            <div class="row">
                <div class="col-md-12 list-eventsurport">
                    <h2 class="fonts-h"><?php echo get_the_title(652)?></h2>
                    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
                    <script type="text/javascript">
                        function showonlyonediv(thechosenone) {
                            $('.newboxes').each(function (index) {
                                if ($(this).attr("id") == thechosenone) {
                                    $(this).show(200);
                                }
                                else {
                                    $(this).hide(600);
                                }
                            });
                        }
                    </script>
                    <div class="wrapper-surport">


                        <?php
                        global $post;
                        $array = array('category_name' => 'active-surports', 'orderrand' => 'desc', 'numberposts' => '10');
                        $result = get_posts($array);
                        $i = 1;
                        foreach ($result as $post): setup_postdata($post);
                            ?>
                            <div class="round">
                                <div class="title">
                                    <a id="myHeader<?php echo $i; ?>"
                                       href="javascript:showonlyonediv('newboxes<?php echo $i; ?>');">
                                       <div class="a"> <i class="fa fa-angle-double-down"></i> <?php echo the_title(); ?></div></a>
                                </div>
                                <div class="newboxes" id="newboxes<?php echo $i; ?>" style="display:<?php if ($i == 1) {
                                    echo "block;";
                                } else {
                                    echo "none";
                                } ?> ">
                                    <?php echo the_content(); ?>
                                </div>
                            </div>
                            <?php $i = $i + 1; ?>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>