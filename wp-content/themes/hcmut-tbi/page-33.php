<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="page-main" class="events-pages">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="show-details-page">
                    <div class="show-slide">
                        <?php echo do_shortcode('[ngg_images gallery_ids="3" images=3 display_type="photocrati-nextgen_basic_slideshow" gallery_width="800" gallery_height="400"]');?>
                    </div>
                    <?php

                    while (have_posts()) : the_post();
                        echo the_content();
                    endwhile;
                    ?>
                </div>

            </div>
        </div>
    </div>
<?php

get_footer();
