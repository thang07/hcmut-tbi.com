<?php
/**
 * The template for displaying a single event
 *
 * Please note that since 1.7, this template is not used by default. You can edit the 'event details'
 * by using the event-meta-event-single.php template.
 *
 * Or you can edit the entire single event template by creating a single-event.php template
 * in your theme. You can use this template as a guide.
 *
 * For a list of available functions (outputting dates, venue details etc) see http://codex.wp-event-organiser.com/
 *
 ***************** NOTICE: *****************
 *  Do not make changes to this file. Any changes made to this file
 * will be overwritten if the plug-in is updated.
 *
 * To overwrite this template with your own, make a copy of it (with the same name)
 * in your theme directory. See http://docs.wp-event-organiser.com/theme-integration for more information
 *
 * WordPress will automatically prioritise the template in your theme directory.
 ***************** NOTICE: *****************
 *
 * @package Event Organiser (plug-in)
 * @since 1.0.0
 */

//Call the template header
get_header(); ?>

<div class="single-events">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8">
                    <div id="contents" role="main">

                        <?php while (have_posts()) :
                            the_post(); ?>
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <div class="entry-header">
                                    <h3 class="entry-title"><?php the_title(); ?></h3>
                                </div>

                                <div class="entry-content">
                                    <?php the_post_thumbnail(); ?>
                                    <!-- Get event information, see template: event-meta-event-single.php -->
                                    <?php eo_get_template_part('event-meta', 'event-single'); ?>
                                    <?php the_content(); ?>
                                </div>
                            </article>

                        <?php endwhile; ?>

                    </div>
                    <!-- #content -->
                </div>
                <div class="col-md-4">
                    <div class="slide-can sidebar-nav ">
                        <?php if (isset($_GET['lang'])) { ?>
                            <h2 class="fonts">Calendar</h2>
                        <?php } else { ?>
                            <h2 class="fonts">Lịch</h2>
                        <?php } ?>
                        <?php if (is_active_sidebar('right-sidebar')) echo dynamic_sidebar('right-sidebar'); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div><!-- #primary -->

<!-- Call template footer -->
<?php get_footer(); ?>
