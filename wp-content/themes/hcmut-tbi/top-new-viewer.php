<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 12/11/14
 * Time: 5:00 PM
 * top danh sach tin tuc xem nhieu nhat
 */
?>

<?php
$popularpost = new WP_Query(array('posts_per_page' => 3,
    'meta_key' => 'wpb_post_views_count',
    'orderby' => 'rand',
    'order' => 'DESC'));
if(count($popularpost)>0){
?>

<div class="top-new-viewer">
    <h2 class="fonts-h aligncenter">
        <?php if (isset($_GET['lang'])) { ?>
            popular article
        <?php } else { ?>
            Tin nổi bật
        <?php } ?>
    </h2>

    <ul>

     <?php
        while ($popularpost->have_posts()) : $popularpost->the_post();?>
            <li>
                <a href="<?php echo the_permalink() ?>" class="title">
                    <h5><?php echo wp_trim_words(get_the_title(), 10); ?> </h5></a>

                <div class="img">
                    <a href="<?php echo the_permalink() ?>">
                        <?php echo the_post_thumbnail('small', array('class' => 'attachment-post-thumbnail img-responsive')); ?>
                    </a>
                </div>
                <div class="excerpt-new">

                    <?php
                    $con = get_the_excerpt();
                    $trim = wp_trim_words($con, 15, '..');
                    echo substr($trim, 2);
                    ?>
                </div>
            </li>
        <?php endwhile; ?>
    </ul>
</div>

<?php } ?>

