<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.quick.pagination.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
//        $("ul.pagination1").quickPagination();
        $("ul.pagination1").quickPagination({pageSize: "6", currentPage: 1, holder: null, pagerLocation: "after"});

    });
</script>
<div class="page-category events-pages">
    <div class="container">
        <div class="row">
            <h2 class="fonts"><?php echo single_cat_title('', false); ?></h2>
            <ul class="col-sm-12 cate banner pagination1">

                <?php if (have_posts()) : ?>

                    <?php
                    // Start the Loop.
                    while (have_posts()) : the_post();?>
                        <li class="col-sm-4">
                            <div class="item">
                                <a href="<?php echo the_permalink() ?>" title="<?php echo the_title() ?>">
                                    <?php echo the_post_thumbnail() ?>
                                </a>
                                <h5 class="title"><?php echo wp_trim_words(get_the_title(),7,'...'); ?></h5>
                            </div>
                        </li>
                    <?php
                    endwhile;
                endif;
                ?>
            </ul>
        </div>
    </div>
</div>
<?php get_footer(); ?>
