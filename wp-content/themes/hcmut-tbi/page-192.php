<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 12/10/14
 * Time: 10:10 AM
 * trang partners
 */
get_header()?>
    <div class="partner-page events-pages">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="fonts"><?php echo get_the_title('192'); ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-partner">
                    <?php
                    global $post;
                    $array = array('category_name' => 'partners', 'orderrand' => 'desc', 'numberposts' => '50');
                    $result = get_posts($array);
                    foreach ($result as $post): setup_postdata($post);
                        ?>
                        <div class="col-md-3 item">
                            <div class="logo-part" title="<?php echo the_title() ?>">
                                <?php if (has_post_thumbnail()) { ?>
                                    <a href="<?php echo get_field('url') ?>" title="<?php echo the_title() ?>"
                                       target="_blank">
                                        <?php echo the_post_thumbnail(); ?>
                                    </a>
                                <?php } else { ?>
                                    <a href="<?php echo get_field('url') ?>" title="<?php echo the_title() ?>"
                                       target="_blank">
                                        <?php echo the_title() ?>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>