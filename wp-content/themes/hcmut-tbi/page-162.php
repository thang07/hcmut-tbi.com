<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 12/8/14
 * Time: 9:52 AM
 * trang lien he
 */
get_header()
?>
<div class="page-contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="map-info">
                        <h3 class="fonts-h"><?php  echo get_the_title(162)?></h3>
                        <div class="map-google">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3919.527339802303!2d106.65804680000001!3d10.770863999999998!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752ec1f254dfd9%3A0x8dbac4276e2f2d10!2zMjY4IEzDvSBUaMaw4budbmcgS2nhu4d0LCAxNCwgUXXhuq1uIDEwLCBI4buTIENow60gTWluaCwgVmlldG5hbQ!5e0!3m2!1sen!2s!4v1418014281523" width="100%" height="200" frameborder="0" style="border:0"></iframe>
                        </div>
                        <div class="inf">
                            <?php $page_contact = get_page(162);
                            $content = apply_filters('the_content', $page_contact->post_content);
                            echo $content;
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <?php if (isset($_GET['lang'])) { ?>
                        <h3 class="fonts-h">Contact form</h3>
                        <?php echo do_shortcode("[contact-form-7 id='161' title='Liên hệ']"); ?>
                    <?php } else { ?>
                        <h3 class="fonts-h">Liên hệ</h3>
                        <?php echo do_shortcode("[contact-form-7 id='160' title='Contact form 1']"); ?>

                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

?>
<?php get_footer() ?>
