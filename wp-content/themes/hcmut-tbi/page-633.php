<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 12/25/14
 * Time: 3:20 PM
 */
get_header() ?>
    <div class="page-633 events-pages">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="fonts-h"><?php echo get_the_title(633) ?></h2>
                    <style>

                        fieldset { border: none; width: 100%; }

                        legend { font-size: 18px; margin: 0px; padding: 10px 0px; color: #b0232a; font-weight: bold; }

                        label { display: block; margin: 15px 0 5px; }

                        input[type=text], input[type=password] { width: 300px; padding: 5px; border: solid 1px #000; }

                        .prev, .next { background-color: #b0232a; padding: 5px 10px; color: #fff; text-decoration: none; }

                        .prev:hover, .next:hover { background-color: #000; text-decoration: none; color: #fff }

                        .prev { float: left; }

                        .next { float: right; }

                        #steps { list-style: none; width: 100%; overflow: hidden; margin: 0px; padding: 0px; }

                        #steps li { font-size: 24px; float: left; padding: 10px; color: #b0b1b3; background: #b0232a; color: #fff; margin: 2px; border-radius: 30px; line-height: 15px }

                        #steps li span { display: block; font-size: 16px; padding-left: 3px;margin-top: 2px }

                        #steps li.current { color: #fff; background: #d02a21 }

                        #makeWizard { background-color: #b0232a; color: #fff; padding: 5px 10px; text-decoration: none; font-size: 18px; }

                        .page-633 .fonts-h { margin-bottom: 32px }

                        #makeWizard:hover { background-color: #000; }

                        .steps { background: #dc3e4c; padding: 7px 10px; width: 40%; margin: 0 auto; color: #fff; font-family: OpenSans-Regular; margin-bottom: 20px; position: relative }

                        .steps:before { content: ""; display: block; border: 1.25em solid #dc3e4c;; position: absolute; border-left-color: transparent;
                            bottom: 0em; left: -1.7em; border-left-width: 0.9em; }

                        .steps:after { -moz-border-bottom-colors: none;
                            -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; border-color: transparent transparent transparent #dc3e4c;
                            border-image: none; border-style: solid; border-width: 1.25em; content: ""; display: inline-block;
                            height: 0; position: absolute;
                            right: -34px; top: 0; width: 0;
                        }


                        #SignupForm fieldset {padding-left: 15px;  padding-right: 15px; }
                    </style>
                    <script type="text/javascript"
                            src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
                    <script src="<?php echo get_template_directory_uri(); ?>/js/formToWizard.js"></script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#SignupForm").formToWizard({ submitButton: 'SaveAccount' })
                        });
                    </script>
                    <form id="SignupForm" action="">
                        <?php
                        global $post;
                        $array = array('category_name' => 'selections', 'order' => 'ASC', 'numberposts' => '10');
                        $result = get_posts($array);
                        $i = 1;
                        foreach ($result as $post): setup_postdata($post);
                            ?>
                            <fieldset>
                                <div class="item">
                                    <legend><?php echo the_title() ?></legend>
                                    <div class="show">
                                        <?php echo the_content(); ?>
                                    </div>
                                </div>
                            </fieldset>
                        <?php endforeach ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>