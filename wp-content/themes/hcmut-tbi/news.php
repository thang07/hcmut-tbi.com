<section id="newsall">

    <div class="container round-news cta-text">

        <div class="row">
            <div class=" col-sm-12 col-md-12">
                <h2 class="fonts">
                    <?php if (isset($_GET['lang'])) { ?>
                        <span class="news"><a title="View more" href="<?php echo get_permalink(1035); ?>"> News</a></span> & <a
                          title="View more"  href="<?php echo get_permalink(155); ?>"> Events </a>
                    <?php } else { ?>
                        <a  title="Xem thêm" href="<?php echo get_permalink(1035); ?>"> <span>Tin tức</span></a> & <a
                          title="Xem thêm"  href="<?php echo get_permalink(155); ?>"> Sự kiện </a>
                    <?php } ?>
                </h2>
                <?php

                global $post;
                $array = array('category_name' => 'tin-tuc', 'orderrand' => 'desc', 'numberposts' => '1');
                $result = get_posts($array);
                foreach ($result as $post): setup_postdata($post);
                    ?>
                    <div class="col-sm-6 col-md-6 news">
                        <div class="media post-item">
                            <a href="<?php echo the_permalink() ?>">
                                <?php echo get_the_post_thumbnail(); ?>
                            </a>

                            <div class="media-body">
                                <h3 class="fonts-reds"><a
                                        href="<?php echo the_permalink() ?>"><?php echo wp_trim_words( get_the_title(),11,'..'); ?></a></h3>
                                <div class="details">
                                    <?php echo wp_trim_words(get_the_excerpt(), 18); ?>
                                    <div class="link-more text-right">

                                        <a href=" <?php echo get_permalink(142); ?>">
                                            <?php if (isset($_GET['lang'])) { ?>
                                                <i> View more</i>
                                            <?php } else { ?>
                                                <i>Xem thêm</i>
                                            <?php } ?>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
                <div class="col-sm-6 col-md-6 news">
                    <?php
                    $events = eo_get_events(array(
                        'numberposts' => 1,
                        'order' => 'desc',
                        'showpastevents' => true,//Will be deprecated, but set it to true to play it safe.
                    ));


                    if ($events):
                        foreach ($events as $event):setup_postdata($event);
                            //Check if all day, set format accordingly
                            $format = (eo_is_all_day($event->ID) ? get_option('date_format') : get_option('date_format') . ' ' . get_option('time_format'));
                            // eo_get_the_start($format, $event->ID, null, $event->occurrence_id);
                            ?>
                            <div class="media post-item">
                                <a href="<?php echo get_permalink($event->ID) ?>">
                                    <?php echo get_the_post_thumbnail($event->ID)?>
                                </a>

                                <div class="media-body">
                                    <h3 class="fonts-reds">
                                        <a
                                            href="<?php echo get_permalink($event->ID) ?>">
                                            <?php echo wp_trim_words(get_the_title($event->ID),11,'..'); ?>
                                        </a>
                                    </h3>
                                    <div class="details">
                                        <?php echo wp_trim_words(get_the_excerpt(), 18);?>
                                        <?php

                                        if (eo_is_all_day($event->ID)) {
                                            $format = 'd M Y';
                                            $microformat = 'Y-m-d';
                                        } else {
                                            $format = 'd/m/Y ' ." - " . get_option('time_format');
                                            $microformat = 'c';
                                        }?>
                                        <div itemprop="startDate" class="startDate"
                                              datetime="<?php eo_get_the_start($microformat); ?>">
                                            <i class="fa fa-clock-o"></i> :  <i class="timer-events"><?php echo  eo_get_the_start($format, $event->ID, null, $event->occurrence_id); ?></i>
                                        </div>

                                        <div class="link-more text-right">

                                            <a href=" <?php echo get_permalink(155); ?>">
                                                <?php if (isset($_GET['lang'])) { ?>
                                                    <i> View more</i>
                                                <?php } else { ?>
                                                    <i>Xem thêm</i>
                                                <?php } ?>
                                            </a></div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
        </div>

</section>