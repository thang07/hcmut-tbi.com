<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="page-main" class="events-pages">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="link-page">

                    <?php $id_page=get_the_ID();
                    if($id_page==8 || $id_page==31 || $id_page==83 || $id_page==33 || $id_page==29)
                    { echo "";}else{
                    ?>
                     <i class="fa fa-home"></i>
                    <a href="<?php bloginfo('home'); ?>" class="home"><?php _e('Home'); ?></a> &raquo;
                    <?php if(get_the_title($post->post_parent)!=get_the_title($post)){?>
                        <?php echo get_the_title($post->post_parent);?>
                        &raquo; <?php echo get_the_title($post); ?>
                    <?php }else{?>
                        <?php echo get_the_title($post);?>
                    <?php } ?>

                        <?php } ?>
                </h4>
                <div class="show-details-page">
                    <?php

                    while (have_posts()) : the_post();
                        echo the_content();
                    endwhile;
                    ?>
                </div>

            </div>
        </div>
    </div>
<?php

get_footer();
