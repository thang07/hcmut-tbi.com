<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 11/21/14
 * Time: 10:10 AM
 */

if (!function_exists('hcmut_setup')) :

    function hcmut_setup()
    {

        load_theme_textdomain('hcmut', get_template_directory() . '/languages');

        add_theme_support('automatic-feed-links');

        // Enable support for Post Thumbnails, and declare two sizes.
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'primary' => __('Top primary menu', 'hcmut'),
            'secondary' => __('Secondary menu in left sidebar', 'hcmut'),
        ));

        // Declare support for title theme feature
        add_theme_support( 'title-tag' );

        function sigmatheme_register_widget_sidebars()
        {

            register_sidebar(array(
                'name' => 'right-sidebar',
                'id' => 'right-sidebar',
                'description' => 'Widgets in this area will be shown in the footer.',
                'before_widget' => '<div id="%1$s" class="widget one-third column %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3>',
                'after_title' => '</h3>'
            ));

        }

        add_action('widgets_init', 'sigmatheme_register_widget_sidebars');

        function visiter_register_widget_sidebars()
    {

        register_sidebar (array(
            'name' => 'visiters-sidebar',
            'id' => 'visiters-sidebar',
            'description' => 'Widgets in this area will be shown in the footer.',
            'before_widget' => '<div id="%1$s" class="widget one-third column %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ));

    }

        add_action('widgets_init', 'visiter_register_widget_sidebars');


//Apply do_shortcode() to widgets so that shortcodes will be executed in widgets
        add_filter('widget_text', 'do_shortcode');


        function setPostViews($postID)
        {
            $count_key = 'post_views_count';
            $count = get_post_meta($postID, $count_key, true);
            if ($count == '') {
                $count = 0;
                delete_post_meta($postID, $count_key);
                add_post_meta($postID, $count_key, '0');
            } else {
                $count++;
                update_post_meta($postID, $count_key, $count);
            }
        }

//BEGIN lay so luong truy cap bai viet
        function getPostViews($postID)
        {
            $count_key = 'post_views_count';
            $count = get_post_meta($postID, $count_key, true);
            if ($count == '') {
                delete_post_meta($postID, $count_key);
                add_post_meta($postID, $count_key, '0');
                return "0 View";
            }
            return $count;
        }

//end

    }
endif; //

add_action('after_setup_theme', 'hcmut_setup');

// Declare support for title theme feature
add_theme_support( 'title-tag' );